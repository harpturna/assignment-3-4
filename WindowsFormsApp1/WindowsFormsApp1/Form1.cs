﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string m = textBox1.Text.ToLower();
            string n = textBox2.Text.ToLower();
            string r;


            if (radioButton1.Checked)
            {
                r = "Ryde Pool";
            }
            else
            {
                r = "Ryde Direct";
            }

            if (m == "")
            {
                MessageBox.Show("Please enter your pickup location");
            }
            else if(n == "")
            {
                MessageBox.Show("Please enter your drop location");
            }      
                     
            else if (radioButton1.Checked || radioButton2.Checked)
            {
                if (m == "fairview mall" && n == "tim hortons" || m == "275 yorkland blvd" && n == "cn tower")
                {
                    Form2 form2 = new Form2(m, n, r);
                    form2.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Please enter a valid location");
                }
            }
            else 
            {
                MessageBox.Show("Please Select Ride Type");
            }
        }

            private void radioButton2_CheckedChanged(object sender, EventArgs e)
            {

            }

            private void radioButton1_CheckedChanged(object sender, EventArgs e)
            {

            }
        
    }
}
