﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2(string from, string to, string ryde)
        {
            InitializeComponent();
            label4.Text = from;
            label5.Text = to;
            double baseFare = 2.50;
            double distanceCharge = 0.81;
            double serviceFee = 1.75;
            double distance;
            double total;
          
            if (from == "fairview mall" && to == "tim hortons")
            {
                distance = distanceCharge * 1.2;
                if (ryde == "Ryde Direct")
                {
                    baseFare = baseFare + (baseFare * 10 / 100);
                    distance = distance + (distance * 15 / 100);
                    
                }
            }
            else
            {
                distance = distanceCharge * 22.9;
                if (ryde == "Ryde Direct")
                {
                    baseFare = baseFare + (baseFare * 10 / 100);
                    distance = distance + (distance * 15 / 100);
                    label1.Text = "Ryde Pool";
                }
            }

            if (ryde == "Ryde Direct")
            {
                label1.Text = "Ryde Direct Confirmed";
            }
            else
            {
                label1.Text = "Ryde Pool Confirmed";
            }
            total = baseFare + distance + serviceFee;
            if (total < 5.50)
            {
                total = 5.50;
            }

            TimeSpan time = DateTime.Now.TimeOfDay;
            if ((time >= new TimeSpan(10, 0, 0)) && (time <= new TimeSpan(12, 0, 0)) ||
                (time >= new TimeSpan(16, 0, 0)) && (time <= new TimeSpan(18, 0, 0)) ||
                (time >= new TimeSpan(20, 0, 0)) && (time <= new TimeSpan(21, 0, 0)))
            {
                total = total + (total * 20 / 100);
            }
            label9.Text = "$" + Math.Round(baseFare,2);
            label10.Text = "$" + Math.Round(distance,2);
            label11.Text = "$" + Math.Round(serviceFee,2);
            label13.Text = "$" + Math.Round(total,2);

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
